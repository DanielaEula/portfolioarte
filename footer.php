<?php include "head.php"; ?>

	<footer>
		<div class="row">
		<div class="medium-6 columns"><p>© 2016 by EulaDanielaYamil. All rights reserved</p></div>
		<div class="medium-4 columns">
			<ul>
		
				<li><a href="https://twitter.com/DanielaEula"><i class="fa fa-twitter"></i></a></li>
				<li><a href="https://www.instagram.com/danielaeula_/"><i class="fa fa-instagram"></i></a></li>
				<li><a href="https://es.pinterest.com/Eulaayamil/"><i class="fa fa-pinterest"></i></a></li>
				<li><a href="https://www.linkedin.com/profile/view?id=AAIAABjwTr8Bcuw21W7lmQ9_NsUZGOsykQ3U63Y&trk=nav_responsive_tab_profile"><i class="fa fa-linkedin-square"></i></a></li>
			</ul>
		</div>
	</div>
	</footer>

<script type="text/javascript">

$("a.grouped_elements").fancybox();

$(document).ready(function() {
	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});

</script>

